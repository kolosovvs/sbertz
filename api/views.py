import json
import re

from django.http import JsonResponse
from django.views import View

from api.models import Rule
from sber.settings import redis_client


class EventView(View):

    def post(self, request):
        json_data = json.loads(request.body)
        event = json_data.get("event")
        for rule in Rule.objects.all():
            result_search = re.search(rule.pattern, event)
            if result_search:
                redis_client.lpush(rule.rule_name, event)
                return JsonResponse({'rule_name': rule.rule_name})
        return JsonResponse({'rule_name': None})
