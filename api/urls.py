from django.conf.urls import url

from api.views import EventView

urlpatterns = [
    url("push_event", EventView.as_view(), name="push_event")
]
