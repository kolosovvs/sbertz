from django.db import models


class Rule(models.Model):
    rule_name = models.CharField(max_length=255)
    pattern = models.CharField(max_length=255)

    def __str__(self) -> str:
        return self.rule_name
